import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ifProp } from 'styled-tools'

import {
  Icon, Paragraph, Label
} from '../../../components'

const Wrapper = styled.div`
  position: relative;
  display: flex;
  padding: 1rem;
  box-sizing: border-box;
  opacity: ${ifProp('soon', 0.4, 1)};
  @media screen and (max-width: 640px) {
    padding: 0.5rem;
  }
`

const StyledIcon = styled(Icon)`
  flex: none;
  visibility: hidden;
  margin-top: 16px;
  margin-right: 16px;
  @media screen and (max-width: 640px) {
    width: 32px;
  }

`

const Text = styled.div`
  margin-left: 1rem;
  overflow: auto;
  > :first-child {
    margin: 0;
  }
`



const StyledParagraRight = styled(Label)`
  
    position: absolute;
    right: 0;
    top: 0;
 
`

const Feature = ({
  id, url, icon, link, children, time, author, getArticle, deleteArticle, ...props
}) => {
  const { soon } = props
  return (
    <Wrapper {...props} onClick={() => { window.open(url, '_blank'); }}>
      <Text>
        <Paragraph>
          {children}
          {author && <Label secondary> - {author} - </Label>}
        </Paragraph>
      </Text>

      <StyledParagraRight>
        {time} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {icon && <StyledIcon className="icon" onClick={(ev) => { if (window.confirm('Desea eliminar item?')) { deleteArticle(id); ev.stopPropagation(); } }} icon={icon} width={64} />}
      </StyledParagraRight>

    </Wrapper>
  )
}

Feature.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.string,
  link: PropTypes.string,
  soon: PropTypes.bool,
  children: PropTypes.any,
  code: PropTypes.node,
}

export default Feature
