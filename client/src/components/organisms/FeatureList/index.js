import React, { useState } from 'react'
import styled from 'styled-components'

import {
  Feature, Paragraph, Spinner
} from '../../../components'

const Grid = styled.div`
  display: flex;
  flex-flow: row wrap;
  > * {
    width: calc(100% - 2rem);
    @media screen and (max-width: 640px) {
      width: 100%;
    }
  }
`


const Description = styled(Paragraph)`
  text-align: center;
  margin: 2rem;
  @media screen and (max-width: 640px) {
    margin: 1rem;
  }
`

const StyledFeature = styled(Feature)`
  @media screen and (max-width: 640px) {
    margin: 0;
  }
  border-bottom: 1px solid #ccc;
  &:hover {
    background-color: #fafafa;
    cursor: pointer;
    .icon {
      visibility: visible;
      cursor: pointer;
    }
  }
`

const FeatureList = ({ deleteArticle, articles, ...props }) => {


  return (
    <div {...props}>

      {
        articles ? <Grid>
          {
            articles.map(item => (
              <StyledFeature
                icon="trash"
                link="https://facebook.github.io/react"
                time={item.time}
                author={item.author}
                url={item.url}
                id={item._id}
                deleteArticle={deleteArticle}
              >
                {item.title}
              </StyledFeature>
            ))
          }

        </Grid> : <Spinner />
      }


    </div>
  )
}

export default FeatureList
