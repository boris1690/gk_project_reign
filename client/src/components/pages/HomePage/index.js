// https://github.com/diegohaz/arc/wiki/Atomic-Design
import React, { useEffect, useState } from 'react'

import {
  PageTemplate, Header, Hero, Footer, FeatureList,
} from '../../../components'
import { BASE_URL } from '../../../utils/API';


const HomePage = () => {

  const [articles, setArticles] = useState(null);

  useEffect(() => {
    getArticles()
  }, [])

  const getArticles = async () => {
    setArticles(null);
    const response = await fetch(`${BASE_URL}/article/get`);
    const json = await response.json();
    setArticles(json.articles);
  }

  const deleteArticle = async (id) => {
    const response = await fetch(`${BASE_URL}/article/delete?id=${id}`, {
      method: 'DELETE',
    });
    const json = await response.json();

    await getArticles();
  }

  return (
    <PageTemplate
      header={<Header />}
      hero={<Hero />}
      footer={<Footer />}
    >
      <FeatureList articles={articles} getArticles={getArticles} deleteArticle={deleteArticle} />
    </PageTemplate>
  )
}

export default HomePage
