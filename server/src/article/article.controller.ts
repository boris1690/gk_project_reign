import { Controller, Post, Get, Body, Res, HttpStatus, Delete, Query, NotFoundException } from '@nestjs/common';
import { ArticleService } from './article.service';
import { CreateArticleDTO } from './dto/create-article.dto';

@Controller('article')
export class ArticleController {
    constructor(private articleService: ArticleService) { }

    @Post('/create')
    async addRecipe(@Res() res, @Body() createArticleDTO: CreateArticleDTO) {

        console.log(1234)
        const lists = await this.articleService.addArticle(createArticleDTO);
        return res.status(HttpStatus.OK).json({
            message: "Articl has been created successfully",
            lists
        })

    }

    // Retrieve customers list
    @Get('/get')
    async getAllCustomer(@Res() res) {
        const articles = await this.articleService.getAllArticles();
        return res.status(HttpStatus.OK).json({
            message: "Articles",
            articles
        });
    }

    // Retrieve customers list
    @Delete('/delete')
    async deleteArticle(@Res() res, @Query('id') id) {
        const article = await this.articleService.deleteArticle(id);

        if (!article) throw new NotFoundException('Article does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Article has been deleted',
            article
        })
    }
}
