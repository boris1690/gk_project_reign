import { Injectable, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';
import { ObjectId } from 'mongodb';

import { InjectModel } from '@nestjs/mongoose';
import { Acticle } from './interfaces/article.interface';
import { CreateArticleDTO } from './dto/create-article.dto';
import { map } from 'rxjs/operators';
import * as moment from 'moment';


@Injectable()
export class ArticleService {
    constructor(@InjectModel('Article') private readonly articleModel: Model<Acticle>,
        private readonly httpService: HttpService) { }

    /*
    *   FINDBYID
    *   Get article to ID
    * */

    async findById(id): Promise<Acticle> {
        const article = await this.articleModel.findById(id).exec();
        return article;
    }

    /*
    *   GETALLARTICLES
    *   Get all articles
    * */
    async getAllArticles(): Promise<Acticle[]> {

        await this.syncData();
        const customers = await this.articleModel.find({ status: true }).exec();
        return customers;
    }

    /*
    *   ADDARTICLE
    *   Create Article 
    * */
    async addArticle(createArticleDTO: CreateArticleDTO): Promise<Acticle> {
        const newRecipe = await new this.articleModel(createArticleDTO);
        return newRecipe.save();
    }

    /*
    *   UPDATE ARTICLE
    *   Update Article 
    * */
    async updateArticle(id, createArticleDTO: CreateArticleDTO): Promise<Acticle> {
        return await this.articleModel.findByIdAndUpdate(id, createArticleDTO, { new: true });
    }

    /*
    *   DELETE ARTICLE
    *   DELETE Article - Soft delete set status in false
    * */
    async deleteArticle(id): Promise<Acticle> {

        let createArticleDTO: CreateArticleDTO = { status: false };
        return await this.articleModel.findByIdAndUpdate(id, createArticleDTO, { new: true });
    }

    /*
    *   ASYNCDATA
    *   Get API posts to create or update each one
    * */
    async syncData() {

        let { data } = await this.getExternalApiData();

        const rs = data.hits.map(async (item, idx) => {

            // Validate if exist
            let article = await this.findById(item.objectID);
            let r = null;

            // validate dates
            let today = moment();
            let yesterday = moment().subtract(1, 'day');

            let engagementDate = item.created_at;
            let date = ""


            if (moment(engagementDate, 'YYYY-MM-DDTHH:mm:ss').isSame(today, 'day'))
                date = moment(engagementDate, 'YYYY-MM-DDTHH:mm:ss').format('hh:mm A');
            else if (moment(engagementDate, 'YYYY-MM-DDTHH:mm:ss').isSame(yesterday, 'day'))
                date = "Yesterday";
            else
                date = moment(engagementDate, 'YYYY-MM-DDTHH:mm:ss').format('DD/MMMM');

            // If not exist create new dataset
            if (!article) {

                if (item.title || item.story_title) {

                    let createArticleDTO: CreateArticleDTO = {
                        _id: item.objectID,
                        title: item.title ? item.title : item.story_title,
                        time: date,
                        author: item.author,
                        url: item.url ? item.url : item.story_url,
                        status: true
                    };

                    r = await this.addArticle(createArticleDTO);
                }

            } else {
                let createArticleDTO: CreateArticleDTO = {
                    title: item.title ? item.title : item.story_title,
                    time: date,
                    url: item.url ? item.url : item.story_url
                };

                r = await this.updateArticle(item.objectID, createArticleDTO);
            }

            return r;

        });

        const rtotal = await Promise.all(rs);
        return rtotal;
    }

    /*
    *   GETEXTERNALAPIDATA
    *   Get API posts
    * */
    async getExternalApiData() {

        // Here extra code to inegrate hn posts https://hn.algolia.com/api/v1/search_by_date?query=nodejs
        return await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            }).toPromise();
    }
}
