import { Document, ObjectId } from 'mongoose';
export interface Acticle extends Document {
    _id: String,
    title: String,
    time: String,
    author: String,
    url: String,
    status: Boolean
}