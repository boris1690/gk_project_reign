import { ObjectId } from "mongoose";

export class CreateArticleDTO {
    _id?: String;
    title?: string;
    time?: String;
    author?: String;
    url?: String;
    status?: Boolean;
}