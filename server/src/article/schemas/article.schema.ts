import * as mongoose from 'mongoose';

export const ArticleSchema = new mongoose.Schema({
    _id: String,
    title: String,
    time: String,
    author: String,
    url: String,
    status: Boolean

})