import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleModule } from './article/article.module';
import { ArticleService } from './article/article.service';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://admin:admin123@mongodb:27017/app_post?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false'),
    ArticleModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule { }
