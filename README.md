************* APP POST **************
*   @author Boris Benalcazar        *
*   @date 11-FEB-2021               *
*************************************

STEPS TO START APPLICATION:

1.- Clone the repository.
2.- Enter the root project.
3.- Run "docker-compose build"
4.- Run "docker-compose up"
5.- Open Browser url: http://localhost:3000


